from django.urls import path
from .views import todo_list, show_particular_list, create_list, edit_list, delete_list, create_new_item, edit_item

urlpatterns = [
    path("", todo_list, name="todo_list_list"),
    path("<int:id>/", show_particular_list, name = "todo_list_detail"),
    path("create/", create_list, name = "todo_list_create"),
    path("<int:id>/edit/", edit_list, name = "todo_list_update"),
    path("<int:id>/delete/", delete_list, name = "todo_list_delete"),
    path("items/create/", create_new_item, name = "todo_item_create"),
    path("items/<int:id>/edit/", edit_item, name = "todo_item_update"),
]
