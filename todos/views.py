from django.shortcuts import render, get_object_or_404, redirect
from .models import TodoList, TodoItem
from .forms import ListForm, ItemForm


def todo_list(request):
    todo = TodoList.objects.all()
    context = {
        "todo_list": todo,
    }
    return render(request, "todos/todo_list_list.html", context)


def show_particular_list(request, id):
    a_thing_to_do = get_object_or_404(TodoList, id=id)
    context = {
        "to_do_object": a_thing_to_do,
    }
    return render(request, "todos/todo_list_detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = ListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ListForm()
    context = {
        "form": form,
    }
    return render(request, "todos/todo_list_create.html", context)


def edit_list(request, id):
    post = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = ListForm(request.POST, instance=post)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = ListForm(instance=post)
    context = {
        "post": post,
        "form": form,
    }
    return render(request, "todos/todo_list_update.html", context)


def delete_list(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        model_instance.delete()
        return redirect("todo_list_list")

    return render(request, "todos/todo_list_delete.html")


def create_new_item(request):
    if request.method == "POST":
        form = ItemForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.list.id)
    else:
        form = ItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/todo_item_create.html", context)


def edit_item(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ItemForm(request.POST, instance=post)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_detail", id=form.list.id)
    else:
        form = ItemForm(instance=post)
    context = {
        "post": post,
        "form": form,
    }
    return render(request, "todos/todo_item_update.html", context)
